import threading
import sys
import os
import socket
import readchar
import nltk
import re

import redis
import json

from time import sleep
from src import ioutils


REDIS_STREAM = os.getenv('REDIS_STREAM', 'v1/twitter/pub')
try:
    REDIS_HOST = socket.gethostbyname("redis")
except:
    REDIS_HOST = 'localhost'
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', 'thisisredis')
MAXLEN = os.getenv('MAXLEN', 200)

# class TwitterThread(threading.Thread):
#     def __init__(self, q):
#         threading.Thread.__init__(self)
#         self.q = q
#     def run(self):
#         stream = twitter_stream.MyStreamer(queue=self.q)
#         # MADRID: trends = stream.twython.get_place_trends(id=766273)[-1]
#         trends = stream.twython.get_place_trends(id=2459115)[-1]
#         twython_trends = [e['name'] for e in trends['trends']]
#         logger.info("Starting Stream Statuses collecting from the trends: {}".format(twython_trends))
#         stream.statuses.filter(track=', '.join(twython_trends), language='en')

class RedisFeed:
    def __init__(self, stream_path=REDIS_STREAM, redis_host=REDIS_HOST, redis_port=6379, db=0,
                 password=REDIS_PASSWORD, decode_responses=True, cursor_path='./files/cursor'):
        self.redis_object = redis.Redis(host=redis_host, port=redis_port, db=db,
                                        password=password, decode_responses=decode_responses)
        self.stream_path = stream_path
        self.stream_list = self.update_stream_list(self.stream_path)
        self.cursor_path = cursor_path
        self.min = self.cursor_load()

    def cursor_load(self):
        try:
            with open(self.cursor_path, 'r') as cursor:
                value = cursor.read()
        except FileNotFoundError:
            value = '-'
        return value

    def cursor_update(self, value):
        with open(self.cursor_path, 'w') as cursor:
            cursor.write(value)

    def redis_generator(self):
        self.stream_list = self.update_stream_list(self.stream_path)
        redis_id, redis_dict = self.redis_object.xrange(self.stream_path, min=self.min, max='+', count=2)[1]
        self.min = redis_id
        self.cursor_update(redis_id)
        return redis_dict

    def update_stream_list(self, stream_path):
        scan_result = self.redis_object.scan_iter
        return scan_result

    def redis_recursive_scan(self, cursor=None):
        if cursor == 0:
            cursor, data = self.redis_object.scan(cursor)
            return data
        else:
            cursor, data = self.redis_object.scan(cursor or 0)
            return data + self.redis_recursive_scan(cursor)
class negative_annotate:
    def __init__(self, output_path="./files/negative_annotations.txt", file_mode="a+"):
        self.file = open(output_path, file_mode)
    def write(self, sentence):
        self.file.write("{}\n".format(sentence))
    def close(self):
        self.file.close()
class positive_annotate:
    def __init__(self, output_path="./files/positive_annotations.txt", file_mode="a+"):
        self.file = open(output_path, file_mode)
    def write(self, sentence):
        self.file.write("{}\n".format(sentence))
    def close(self):
        self.file.close()
class neutral_annotate:
    def __init__(self, output_path="./files/neutral_annotations.txt", file_mode="a+"):
        self.file = open(output_path, file_mode)
    def write(self, sentence):
        self.file.write("{}\n".format(sentence))
    def close(self):
        self.file.close()

class create_dict:
    def __init__(self, output_path="./files/annotations.txt", file_mode="a+"):
        self.file = open(output_path, file_mode)
    def write(self, sentence):
        self.file.write("{}\n".format(sentence))
    def close(self):
        self.file.close()

def replace_hashtag(sent, exp = r'(\#[a-zA-Z0-9]+\b)'):
    return re.sub(exp,'#',sent)

def replace_repetitions(sent, exp=r'([a-z])\1+'):
    return re.sub(exp, r'\1', sent)

def replace_metion(sent, exp = r'(\@[a-zA-Z0-9_]+\b)'):
    return re.sub(exp,'@',sent)

def replace_href(sent, exp = r'((http|https)?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.&\-]*)'):
    return re.sub(exp,'href',sent)

def replace_newline(sent):
    return sent.replace("\n", " ")

def main():
    """
    Main function for the module
    """
    def stop(input):
        raise KeyboardInterrupt(input)
    def wrong_key(input):
        logger.info("Ignoring sentence (Input: {})".format(input))
        pass

    negative = negative_annotate()
    neutral = neutral_annotate()
    positive = positive_annotate()
    sentences = create_dict()
    case = {
        '1': negative.write,
        '2': neutral.write,
        '3': positive.write,
        'q': stop
    }
    try:
        redis_object = RedisFeed(redis_host=REDIS_HOST, redis_port=6379, db=0, password=REDIS_PASSWORD)
        # redis_pub_sub = redis_object.pubsub()

        logger.info("Starting python twitter annotator")
        while True:
            o = redis_object.redis_generator()
            for e in nltk.sent_tokenize(o.get('text'), 'spanish'):
                sentence = replace_newline(replace_metion(replace_hashtag(replace_href(e))))
                logger.info("1(-) 2(=) 3(+):\n{}".format(sentence))
                # Read from input a single left/right/up/down arrow click for tweet classification
                # example = readchar.readkey()
                # case.get(example, wrong_key)(sentence)
                sentences.write(sentence)
    except KeyboardInterrupt:
        logger.info("Interrupt key (Q) pressed.")
        negative.close()
        neutral.close()
        positive.close()
        raise KeyboardInterrupt
    # except:
    #     logger.exception("{}".format(sys.exc_info()[0]))



if __name__ == "__main__":
    ## Arguments for the module
    args = ioutils.arguments_init()

    ## Logger
    logger = ioutils.logger_init(log_level=args['log_level'], log_path=args['log_path'])
    while True:
        try:
            main()
        except KeyboardInterrupt:
            logger.info("Exiting")
            break
