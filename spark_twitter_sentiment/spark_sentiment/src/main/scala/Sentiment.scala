import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Encoders, SparkSession}
import org.apache.spark.sql.functions._
import io.lettuce.core._
import edu.stanford.nlp.pipeline._
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations
import edu.stanford.nlp.ling.CoreAnnotations
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations
import java.util.Properties
import scala.collection.JavaConverters._

object SentimentAnalysis {
  def get(text: String) = {
    val props = new Properties()
    props.setProperty("annotators", "tokenize, ssplit, parse, sentiment")

    val pipeline: StanfordCoreNLP = new StanfordCoreNLP(props)
    def getSentimentScore(text: String): Double = { 
      val annotation: Annotation = pipeline.process(text)
      val sentences = annotation.get(classOf[CoreAnnotations.SentencesAnnotation]).asScala.toList.filterNot(x=> x.toString.trim.isEmpty)
      val scoreList = sentences.map(sentence =>
        (sentence, sentence.get(classOf[SentimentCoreAnnotations.SentimentAnnotatedTree])))
        .map{case(sentence, tree) => (sentence.toString,RNNCoreAnnotations.getPredictedClass(tree))}
        .toList
      val scores = scoreList.map(_._2)
      scores.sum.toDouble/scores.length
    }
    getSentimentScore(text)
  }
}

object Sentiment {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("Spark basic example") // Name for the session
      .master("local[4]") // Path and number of cores to be used
      .config("spark.redis.host", "redis")
      .config("spark.redis.port", "6379")
      .config("spark.redis.auth", "thisisredis")
      .getOrCreate()
    // Optional: Set logging level if log4j not configured
    Logger.getRootLogger.setLevel(Level.WARN)

    val sc = spark.sparkContext
    val colsSent = udf {x : String => SentimentAnalysis.get(x)}

    val redisClient = RedisClient.create("redis://thisisredis@redis:6379/0")
    val connection = redisClient.connect
    val syncCommands = connection.sync
    var last_key = "0-0"

    import spark.implicits._

    while (true) {
      val now = System.nanoTime
      val output = syncCommands.xread(XReadArgs.StreamOffset.from("v1/twitter/pub", last_key)).asScala.toList
      last_key = output.map(x => (x.getId())).max
    
      val body = sc.parallelize(output.map(x => (x.getBody())))
      val cols = body.take(1).flatMap(x => x.asScala.keys)
      var df = body.map { 
        value => 
          val list = value.values.asScala.toList
          (list(0), list(1), list(2), list(3), list(4), list(5), list(6), list(7), list(8)) // To improve
      }.toDF(cols:_*)
      
      df = df.withColumn("sentiment", colsSent(col("text")))
      df.select(mean($"sentiment")).show
      println("Time spent to analyze " + output.length + " twits: " + ((System.nanoTime - now) * 1e-6).toInt + "ms")
      //TODO: Do operations to add to the current df
    }

    connection.close
    spark.close()
  }
}