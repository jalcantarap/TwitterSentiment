name := "Hello World"

version := "0.1"

scalaVersion := "2.12.11"

javaOptions += "-Xmx4G"

resolvers += Resolver.url("bintray-spark-packages", url("https://dl.bintray.com/spark-packages/maven/"))(Resolver.ivyStylePatterns)

libraryDependencies ++= Seq("org.apache.spark" %% "spark-sql" % "2.4.5",
  "org.apache.spark" %% "spark-core" % "2.4.5")
libraryDependencies += "edu.stanford.nlp" % "stanford-corenlp" % "4.0.0"
libraryDependencies += "edu.stanford.nlp" % "stanford-corenlp" % "4.0.0" classifier "models"
libraryDependencies += "io.lettuce" % "lettuce-core" % "6.0.0.M1"
