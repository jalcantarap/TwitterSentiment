import threading
import sys
import os
import socket
import readchar
import nltk
import gensim
import re
import string
import json
import numpy as np
import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from src import ioutils

# torch.cuda.is_available() checks and returns a Boolean True if a GPU is available, else it'll return False
is_cuda = torch.cuda.is_available()

# If we have a GPU available, we'll set our device to GPU. We'll use this device variable later in our code.
if is_cuda:
    DEVICE = torch.device("cuda")
else:
    DEVICE = torch.device("cpu")

#https://blog.floydhub.com/long-short-term-memory-from-zero-to-hero-with-pytorch/
class TweetLSTM(nn.Module):
    def __init__(self, hidden_dim, output_size, n_layers, device=DEVICE,
                 drop_lstm=0.1, drop_out=0.1, embedding_model_path="./models/complete_model_spanish/complete.model"):
        super().__init__()
        self.device = device
        self.output_size = output_size
        self.n_layers = n_layers
        self.hidden_dim = hidden_dim

        # embedding
        self.word2vec = gensim.models.KeyedVectors.load_word2vec_format(embedding_model_path, binary=True)
        self.embedding = SentenceEmbedding(self.word2vec)

        # LSTM layers
        self.lstm = nn.LSTM(self.word2vec.vector_size, hidden_dim, n_layers,
                            dropout=drop_lstm, batch_first=True)

        # dropout layer
        self.dropout = nn.Dropout(drop_out)

        # linear and sigmoid layers
        self.fc = nn.Linear(hidden_dim, output_size)
        self.sig = nn.Sigmoid()

    def forward(self, x, seq_lengths):
        # embeddings
        embedded_seq_tensor = self.embedding(x)

        # pack, remove pads
        packed_input = pack_padded_sequence(embedded_seq_tensor, seq_lengths.cpu().numpy(), batch_first=True)

        # lstm
        packed_output, (ht, ct) = self.lstm(packed_input, None)
        # https://pytorch.org/docs/stable/_modules/torch/nn/modules/rnn.html
        # If `(h_0, c_0)` is not provided, both **h_0** and **c_0** default to zero

        # unpack, recover padded sequence
        output, input_sizes = pad_packed_sequence(packed_output, batch_first=True)

        # collect the last output in each batch
        last_idxs = (input_sizes - 1).to(self.device)  # last_idxs = input_sizes - torch.ones_like(input_sizes)
        output = torch.gather(output, 1, last_idxs.view(-1, 1).unsqueeze(2).repeat(1, 1,
                                                                                   self.hidden_dim)).squeeze()  # [batch_size, hidden_dim]

        # dropout and fully-connected layer
        output = self.dropout(output)
        output = self.fc(output).squeeze()

        # sigmoid function
        output = self.sig(output)

        return output


from time import sleep, time
from nltk.stem import SnowballStemmer
REDIS_STREAM = os.getenv('REDIS_STREAM', 'v1/twitter/pub')
try:
    REDIS_HOST = socket.gethostbyname("redis")
except:
    REDIS_HOST = 'localhost'
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', 'thisisredis')
MAXLEN = os.getenv('MAXLEN', 200)
## Model from https://zenodo.org/record/1410403#.XvuCLpaxVH4 https://github.com/aitoralmeida/spanish_word2vec
## From https://rare-technologies.com/word2vec-tutorial/
## Word embeddings with PCA https://machinelearningmastery.com/develop-word-embeddings-python-gensim/
## Lemmatize https://textminingonline.com/dive-into-nltk-part-iv-stemming-and-lemmatization
class SentenceIterator(object):
    def __init__(self, file_path):
        self.file_path = file_path
        # self.stemmer = SnowballStemmer("spanish")

    def vectorize(self, text):
        #text = re.sub(r"[^\x00-\x7F\xE1\xE9\xED\xF3\xFA\xC1]+", " ", text)
        regex = re.compile('[' + re.escape(string.punctuation) + '0-9\\r\\t\\n]')  # remove punctuation and numbers
        nopunct = regex.sub(" ", text.lower())
        # return [self.stemmer.stem(w) for w in nltk.word_tokenize(nopunct, 'spanish')]
        return nltk.word_tokenize(nopunct, 'spanish')

    def __iter__(self):
        for line in open(self.file_path):
            yield self.vectorize(line)


class SentenceEmbedding(object):
    def __init__(self, model):
        self.model = model

    def apply_model_to_token(self, token):
        try:
            return np.array(self.model.wv[[token]])
        except KeyError:
            return np.zeros(self.model.vector_size).reshape(1,400)

    def apply_model(self, token):
        a,b = len(token), 400
        token_result = np.zeros(a*b).reshape(a,b)
        for i, t in enumerate(token):
            aux = self.apply_model_to_token(t)
            token_result[i] = aux[0]
        return token_result

    def token2vector(self, token):
        result = (lambda t: self.apply_model(t))(np.array(token))
        return result

    def tokenize_text(self, text):
        #text = re.sub(r"[^\x00-\x7F\xE1\xE9\xED\xF3\xFA\xC1]+", " ", text)
        regex = re.compile('[' + re.escape(string.punctuation) + '0-9\\r\\t\\n]')  # remove punctuation and numbers
        nopunct = regex.sub(" ", text.lower())
        # return [self.stemmer.stem(w) for w in nltk.word_tokenize(nopunct, 'spanish')]
        return nltk.word_tokenize(nopunct, 'spanish')

    def __call__(self, sentence):
        # Batch file reading
        # positive_sentences = SentenceIterator(os.path.join(self.annotations_dir, "positive_annotations.txt"))
        # Batch sentence to word tokenize
        # Word to Vector Embedding
        tokenized_sentence = self.tokenize_text(sentence)
        result = self.token2vector(tokenized_sentence)
        # logger.info("sentence: {} - shape: {}".format(sentence, result.shape))
        return result


# class RedisFeed:
#     def __init__(self, stream_path=REDIS_STREAM, redis_host=REDIS_HOST, redis_port=6379, db=0,
#                  password=REDIS_PASSWORD, decode_responses=True, cursor_path='./files/cursor'):
#         self.redis_object = redis.Redis(host=redis_host, port=redis_port, db=db,
#                                         password=password, decode_responses=decode_responses)
#         self.stream_path = stream_path
#         self.stream_list = self.update_stream_list(self.stream_path)
#         self.cursor_path = cursor_path
#         self.min = self.cursor_load()
#
#     def cursor_load(self):
#         try:
#             with open(self.cursor_path, 'r') as cursor:
#                 value = cursor.read()
#         except FileNotFoundError:
#             value = '-'
#         return value
#
#     def cursor_update(self, value):
#         with open(self.cursor_path, 'w') as cursor:
#             cursor.write(value)
#
#     def redis_generator(self):
#         self.stream_list = self.update_stream_list(self.stream_path)
#         redis_id, redis_dict = self.redis_object.xrange(self.stream_path, min=self.min, max='+', count=2)[1]
#         self.min = redis_id
#         self.cursor_update(redis_id)
#         return redis_dict
#
#     def update_stream_list(self, stream_path):
#         scan_result = self.redis_object.scan_iter
#         return scan_result
#
#     def redis_recursive_scan(self, cursor=None):
#         if cursor == 0:
#             cursor, data = self.redis_object.scan(cursor)
#             return data
#         else:
#             cursor, data = self.redis_object.scan(cursor or 0)
#             return data + self.redis_recursive_scan(cursor)
# class negative_annotate:
#     def __init__(self, output_path="./files/negative_annotations.txt", file_mode="a+"):
#         self.file = open(output_path, file_mode)
#     def write(self, sentence):
#         self.file.write("{}\n".format(sentence))
#     def close(self):
#         self.file.close()
# class positive_annotate:
#     def __init__(self, output_path="./files/positive_annotations.txt", file_mode="a+"):
#         self.file = open(output_path, file_mode)
#     def write(self, sentence):
#         self.file.write("{}\n".format(sentence))
#     def close(self):
#         self.file.close()
# class neutral_annotate:
#     def __init__(self, output_path="./files/neutral_annotations.txt", file_mode="a+"):
#         self.file = open(output_path, file_mode)
#     def write(self, sentence):
#         self.file.write("{}\n".format(sentence))
#     def close(self):
#         self.file.close()
#
# def replace_hashtag(sent, exp = r'(\#[a-zA-Z0-9]+\b)'):
#     return re.sub(exp,'#',sent)
#
# def replace_repetitions(sent, exp=r'([a-z])\1+'):
#     return re.sub(exp, r'\1', sent)
#
# def replace_metion(sent, exp = r'(\@[a-zA-Z0-9_]+\b)'):
#     return re.sub(exp,'@',sent)
#
# def replace_href(sent, exp = r'((http|https)?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.&\-]*)'):
#     return re.sub(exp,'href',sent)
#
# def replace_newline(sent):
#     return sent.replace("\n", " ")

def main():
    """
    Main function for the module
    """
    s = SentencesPipeline('/home/jaime/PycharmProjects/TwitterSentiment/python_twitter_wordembedding/files')

    # except:
    #     logger.exception("{}".format(sys.exc_info()[0]))



if __name__ == "__main__":
    ## Arguments for the module
    args = ioutils.arguments_init()

    ## Logger
    logger = ioutils.logger_init(log_level=args['log_level'], log_path=args['log_path'])
    # while True:
    try:
        main()
    except KeyboardInterrupt:
        logger.info("Exiting")
        # break
