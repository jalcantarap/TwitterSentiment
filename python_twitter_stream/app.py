import threading
import sys
import os
import socket

import redis
import json

from time import sleep
from src import twitter_stream, ioutils

import requests

REDIS_STREAM = os.getenv('REDIS_STREAM', 'v1/twitter/pub')
try:
    REDIS_HOST = socket.gethostbyname("redis")
except:
    REDIS_HOST = 'localhost'
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', 'thisisredis')
MAXLEN = os.getenv('MAXLEN', int(1e9))

class TwitterThread(threading.Thread):
    def __init__(self, q):
        threading.Thread.__init__(self)
        self.q = q
    def run(self):
        stream = twitter_stream.MyStreamer(queue=self.q)
        # trends = stream.twython.get_place_trends(id=766273)[-1]
        trends = stream.twython.get_place_trends(id=2459115)[-1]
        twython_trends = [e['name'] for e in trends['trends']]
        # twython_trends = [":)", ":-)", "😍"]
        logger.info("Starting Stream Statuses collecting from the trends: {}".format(twython_trends))
        try:
            stream.statuses.filter(track=', '.join(twython_trends), language='en')
        except requests.exceptions.ChunkedEncodingError:
            raise Exception("Thread must be restarted")

def main():
    """
    Main function for the module
    """
    try:
        redis_object = redis.Redis(host=REDIS_HOST, port=6379, db=0, password=REDIS_PASSWORD)
        # redis_pub_sub = redis_object.pubsub()

        q = {}
        logger.info("Starting python twitter stream thread")
        thread = TwitterThread(q=q)
        thread.setDaemon(True)
        thread.start()

        while True:
            sleep(5)
            logger.info("Queue length: {}".format(json.dumps(len(q))))
            for e in q.copy():
                item = q.pop(e)
                redis_item = {k:str(v) for k, v in item.items()}
                redis_object.xadd(name=REDIS_STREAM, fields=redis_item, maxlen=MAXLEN)

    except:
        logger.exception("{}".format(sys.exc_info()[0]))



if __name__ == "__main__":
    ## Arguments for the module
    args = ioutils.arguments_init()

    ## Logger
    logger = ioutils.logger_init(log_level=args['log_level'], log_path=args['log_path'])
    while True:
        try:
            main()
        except KeyboardInterrupt:
            logger.info("Exiting")
