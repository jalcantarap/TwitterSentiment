# Twitter Sentiment Analysis

## Building
*The following docker-compose commands have been tested for ubuntu 20.04*. If you can't run it remove volumes in docker-compose.yml file (you won't store anything apart from the container).

```bash
docker-compose build
```

## Run
```bash
docker-compose up

```
## Rebuild
```bash
docker-compose down
docker-compose build
```